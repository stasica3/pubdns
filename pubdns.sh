#!/bin/sh

# query well known public dns resolvers
# and my own dns resolvers
#
# 2016-05-15 arsen.stasic@univie.ac.at initial setup
# 2016-06-08 adding my own dns resolvers
# 2016-06-27 adding some comments before pushing to git ;-)
#
#dyndns unbound: https://ripe72.ripe.net/archives/video/231/ 6:40
#ipv4 for dns-oarc is not working
#184.105.193.73#bind.odvr.dns-oarc.net
#184.105.193.74#unbound.odvr.dns-oarc.net


# define max length for pretty-printing output
MAXLEN='resolver1.dyndnsinternetguide.com  '

# list of public dns resolvers
PUBRESOLVERS='
8.8.8.8[google-public-dns-a.google.com]
216.146.35.35[resolver1.dyndnsinternetguide.com]
208.67.222.222[resolver1.opendns.com]
156.154.71.1[rdns2.ultradns.net]
4.2.2.2[b.resolvers.Level3.net]
2620:ff:c000:0:1:0:64:20[bind.odvr.dns-oarc.net]
2620:ff:c000:0:1:0:64:21[unbound.odvr.dns-oarc.net]
37.235.1.177[resolver2.freedns.zone]
64.6.64.6[recpubns1.nstld.net]'

# my own dns resolvers
MYRESOLVERS='
127.0.0.1[bind-localhost]
127.0.0.2[unbound-localhost]
127.0.0.3[knot-resolver-localhost]
127.0.0.4[pdns-recursor-localhost]
'

HOSTNAME=$( hostname )
MYPC='pcas'
RES=''

# test if I'm unsing my own PC, because I multiple resolvers installed
if [ "$HOSTNAME" = "$MYPC" ]; then
	RES="$PUBRESOLVERS $MYRESOLVERS"
else
	RES=$PUBRESOLVERS
fi

# test if dig is available
command -v dig >/dev/null 2>&1 || { echo "dig is required but not installed. Aborting." >&2; exit 1; }

for i in $RES ; do
	# echo "\t$i"
	RES=$( echo $i | awk -F'[' '{ print $1 }' )
	NAM=$( echo $i | awk -F'[' '{ print $2 }' )
	LEN=${#NAM};
	# echo "length of $RES: $LEN";
	DIF=$( expr ${#MAXLEN} - ${LEN} );
	echo -n "$(tput setaf 3)${NAM}$(tput sgr0) ";
	printf '.%.0s' $(seq 1 $DIF);
	#echo $RES
	dig @$RES $*
done
